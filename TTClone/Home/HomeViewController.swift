//
//  HomeViewController.swift
//  TTClone
//
//  Created by tyfoo on 02/11/2019.
//  Copyright © 2019 tyf. All rights reserved.
//

import UIKit
import Koloda
import SDWebImage
protocol DetailDelegate {
    func onLike()
    func onSkip()
    func onDismiss()
}
class HomeViewController: UIViewController {
    
    @IBOutlet weak var kolodaView: KolodaView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    let networkManager = NetworkManager()
    
    var images:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        kolodaView.delegate = self
        kolodaView.dataSource = self
        
        indicator.startAnimating()
        networkManager.fetchImages { (datas, err) in
            
            self.indicator.stopAnimating()
            guard let datas = datas, datas.count > 0 else {
                print(#function, "No data returned")
                return
            }
            
            for data in datas {
                self.images.append(data.url)
            }
            print("Total Item: \(datas.count)")
            self.kolodaView.reloadData()
        }
        
    }
    
    // Downsampling large images for display at smaller size
    func resizedImage(imageAt imageURL: URL, to pointSize: CGSize, scale: CGFloat) -> UIImage {
        
        let imageSourceOptions = [kCGImageSourceShouldCache: false] as CFDictionary
        let imageSource = CGImageSourceCreateWithURL(imageURL as CFURL, imageSourceOptions)!
        let maxDimensionInPixels = max(pointSize.width, pointSize.height) * scale
        
        let downsampleOptions =  [kCGImageSourceCreateThumbnailFromImageAlways: true,
                                  kCGImageSourceShouldCacheImmediately: true,
                                  kCGImageSourceCreateThumbnailWithTransform: true,
                                  kCGImageSourceThumbnailMaxPixelSize: maxDimensionInPixels] as CFDictionary
        
        let downsampledImage =   CGImageSourceCreateThumbnailAtIndex(imageSource, 0, downsampleOptions)!
        return UIImage(cgImage: downsampledImage)
    }
    
    @IBAction func onSkipTap(_ sender: Any) {
        kolodaView.swipe(.left)
    }
    
    
    @IBAction func onLikeTap(_ sender: Any) {
        kolodaView.swipe(.right)
    }
    
}
extension HomeViewController: KolodaViewDelegate {
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        koloda.reloadData()
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        let vc = UserDetailViewController(nibName: "UserDetailViewController", bundle: nil)
        vc.configure(url: images[index], index: index)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
}
extension HomeViewController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        return images.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let vc = Bundle.main.loadNibNamed("CustomOverlayView", owner: self, options: nil)?[0] as! CustomOverlayView
        vc.configure(urlString: images[index], index: index)
        return vc
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return nil
    }
}
extension HomeViewController: DetailDelegate {
    func onLike() {
        kolodaView.swipe(.right)
    }
    
    func onSkip() {
        kolodaView.swipe(.left)
    }
    
    func onDismiss() {
        
    }
    
    
}
