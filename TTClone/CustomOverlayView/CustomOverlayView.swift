//
//  CustomOverlayView.swift
//  TTClone
//
//  Created by tyfoo on 02/11/2019.
//  Copyright © 2019 tyf. All rights reserved.
//

import UIKit
import Koloda

private let overlayRightImageName = "overlay_like"
private let overlayLeftImageName = "overlay_skip"

class CustomOverlayView: OverlayView {

    @IBOutlet weak var overlayImageView: UIImageView!
    
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var gender: UILabel!
    
    @IBOutlet weak var horoscope: UILabel!
    
    @IBOutlet weak var occupation: UILabel!
    
    private var shadowLayer: CAShapeLayer!
    private var cornerRadius: CGFloat = 25.0
    private var fillColor: UIColor = .clear
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = fillColor.cgColor
            
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            shadowLayer.shadowOpacity = 0.5
            shadowLayer.shadowRadius = 3
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
    func configure(urlString: String, index: Int) {
        
        overlayImageView.hero.id = "image-\(index)"
        name.hero.id = "name-\(index)"
        gender.hero.id = "gender-\(index)"
        horoscope.hero.id = "horoscope-\(index)"
        occupation.hero.id = "occupation-\(index)"
        
        if let url = URL(string: urlString) {
            self.overlayImageView.sd_setImage(with: url, placeholderImage: nil, options: [.scaleDownLargeImages, .continueInBackground], completed: nil)
        }
    }
    
    
}
