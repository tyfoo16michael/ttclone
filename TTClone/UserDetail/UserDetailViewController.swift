//
//  UserDetailViewController.swift
//  TTClone
//
//  Created by tyfoo on 02/11/2019.
//  Copyright © 2019 tyf. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {
    
    @IBOutlet weak var skipBtn: UIButton!
    
    @IBOutlet weak var likeBtn: UIButton!
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var preview: UIImageView!
    
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var gender: UILabel!
    
    @IBOutlet weak var horoscope: UILabel!
    
    @IBOutlet weak var occupation: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    
    var delegate: DetailDelegate?
    
    var index: Int = 0
    var url: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hero.isEnabled = true
        self.view.hero.isEnabledForSubviews = true
        preview.hero.id = "image-\(index)"
        name.hero.id = "name-\(index)"
        gender.hero.id = "gender-\(index)"
        horoscope.hero.id = "horoscope-\(index)"
        occupation.hero.id = "occupation-\(index)"
        
        guard let url = URL(string: url) else {
            return
        }
        preview.sd_setImage(with: url, completed: nil)
        
        likeBtn.hero.modifiers = [.translate(x: 100, y: 0, z: 0), .fade]
        skipBtn.hero.modifiers = [.translate(x: -100, y: 0, z: 0), .fade]
        
        backBtn.hero.modifiers = [.translate(x: 0, y: 100, z: 0), .fade]
        
        stackView.hero.modifiers = [.translate(x: 0, y: 300, z: 0), .fade]
        
        likeBtn.addTarget(self, action: #selector(likeTap), for: .touchUpInside)
        
        skipBtn.addTarget(self, action: #selector(skipTap), for: .touchUpInside)
        
        backBtn.addTarget(self, action: #selector(backTap), for: .touchUpInside)
    }
    
    func configure(url: String, index: Int) {
        self.index = index
        self.url = url
    }
    
    @objc func likeTap() {
        dismiss(animated: true, completion: {
            self.delegate?.onLike()
        })
    }
    
    @objc func skipTap() {
        
        dismiss(animated: true, completion: {
            self.delegate?.onSkip()
        })
    }
    
    @objc func backTap() {
        
        dismiss(animated: true, completion: {
            self.delegate?.onDismiss()
        })
    }
}
