//
//  NetworkManager.swift
//  TTClone
//
//  Created by tyfoo on 02/11/2019.
//  Copyright © 2019 tyf. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
struct ImageData {
//    "id": "0",
//    "author": "Alejandro Escamilla",
//    "width": 5616,
//    "height": 3744,
//    "url": "https://unsplash.com/...",
//    "download_url": "https://picsum.photos/..."
    let id : String
    let url : String
    
    init(json: JSON) {
        self.id = json["id"].string ?? ""
        self.url = json["download_url"].string ?? ""
    }
    
}
class NetworkManager {
    
    func fetchImages(completion: @escaping ([ImageData]?, Error?) -> Void) {
        
        let urlString = "https://picsum.photos/v2/list?limit=30"
        guard let url = URL(string: urlString) else {
            print("Invalid URL")
            completion(nil, nil)
            return
        }
        
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { (response) in
            
            guard response.result.isSuccess else {
                print(#function, "Error while fetching images: \(String(describing: response.result.error))")
                completion(nil, nil)
                return
            }
            
            guard let value = response.result.value else {
                completion(nil, nil)
                print("Invalid URL")
                return
            }
            let json = JSON(value)
            
            let imgs: [ImageData] = json.arrayValue.map({ (json) -> ImageData in
                return ImageData(json: json)
            })
            
            completion(imgs, nil)
            
        }
        
    }
    
}
